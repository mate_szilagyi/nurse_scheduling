import numpy as np
# changes one gene ( one nurse's schedule )
def reset(matrix):
    patterns = [[0], [1, 0, 0], [1, 1, 0, 0, 0]]
    probability = ['0.3', '0.6', '0.1']
    col = matrix.shape[1]
    l = []
    flat_list = []

    while flat_list.__len__() < col:
        l.append(np.random.choice(patterns, p=probability))
        flat_list = [item for sublist in l for item in sublist]

    rand_row = np.random.randint(0, matrix.shape[0])
    matrix[rand_row] = np.array(flat_list[0:col], dtype='int')

    return matrix

def scramble(matrix):
    aux = np.array(matrix)
    r1, r2 = np.random.randint(0, matrix.shape[1], 2)
    while r2 <= r1:
        r1, r2 = np.random.randint(0, matrix.shape[1], 2)

    rand_perm = np.random.permutation(r2 - r1 + 1) + r1
    l = len(rand_perm)
    j = r1
    for i in range(0, l):
        matrix[:, j] = aux[:, rand_perm[i]]
        j += 1

    return matrix

# swap two columns
def swap(matrix):
    c1, c2 = np.random.randint(0, matrix.shape[1], 2)
    matrix[:, [c1, c2]] = matrix[:, [c2, c1]]
    return matrix



# counts 1-s by column, randomly changes 1 to 0 where maximum 1s are; same for 0s
# this helps to balance out shifts
def balance_cols(matrix):
    max = -1
    min = matrix.shape[0]
    for i in range(0, matrix.shape[1]):
        sum_ = sum(matrix[:, i])
        if sum_ >= max:
            max = sum_
            maxp = i
        if sum_ <= min:
            min = sum_
            minp = i
    while True:
        x = np.random.randint(0, matrix.shape[0], size=1, dtype=int)
        if matrix[x, maxp] == 1:
            matrix[x, maxp] = 0
            break
    while True:
        x = np.random.randint(0, matrix.shape[0], size=1, dtype=int)
        if matrix[x, minp] == 0:
            matrix[x, minp] = 1
            break
    return matrix


def balance_rows(matrix):
    max = -1
    min = matrix.shape[1]
    for i in range(0, matrix.shape[0]):
        sum_ = sum(matrix[i, :])
        if sum_ >= max:
            max = sum_
            maxp = i
        if sum_ <= min:
            min = sum_
            minp = i
    while True:
        x = np.random.randint(0, matrix.shape[1], size=1, dtype=int)
        if matrix[maxp, x] == 1:
            matrix[maxp, x] = 0
            break
    while True:
        x = np.random.randint(0, matrix.shape[1], size=1, dtype=int)
        if matrix[minp, x] == 0:
            matrix[minp, x] = 1
            break
    return matrix

def balance(matrix):
    matrix = balance_cols(matrix)
    matrix =balance_rows(matrix)
    return matrix

'''
# swaps 2 genes at random
def swap_mutation_by_row(matrix):
    r1, r2 = np.random.randint(0, matrix.shape[0], 2)
    matrix[[r1, r2]] = matrix[[r2, r1]]
    return matrix


def scramble_mutation_rows(matrix):
    aux = np.array(matrix)
    r1, r2 = np.random.randint(0, matrix.shape[0], 2)
    while r2 <= r1:
        r1, r2 = np.random.randint(0, matrix.shape[0], 2)

    rand_perm = np.random.permutation(r2 - r1 + 1) + r1
    l = len(rand_perm)
    j = r1
    for i in range(0, l):
        matrix[j] = aux[rand_perm[i]]
        j += 1

    return matrix
'''