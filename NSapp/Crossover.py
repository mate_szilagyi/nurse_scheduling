import numpy as np

# Crossover 1 Takes half of the solution (columns) from parent P1 and half of the solution from parent P2
# to create a child
def one_point(parent1, parent2):
    # one point
    randindex = np.random.randint(parent1.shape[1] / 3, size=2)
    child1 = parent1
    child2 = parent2
    for i in range(1, randindex[0] * 3 - 1):
        child2[:, i] = parent1[:, i]
        child1[:, i] = parent2[:, i]
    return np.array(child1, dtype='int'), np.array(child2, dtype='int')


def uniform(parent1, parent2):
    randindex = np.random.uniform(0, 1, size=parent1.shape[1])
    child1 = parent1
    child2 = parent2
    for i in range(0, parent1.shape[1]):
        if randindex[i] > 0.5:
            child1[:, i] = parent2[:, i]
            child2[:, i] = parent1[:, i]
    return np.array(child1, dtype='int'), np.array(child2, dtype='int')

'''
def crossover_uniform_rows(parent1, parent2):
    randindex = np.random.uniform(0, 1, size=parent1.shape[0])
    child1 = parent1
    child2 = parent2
    for i in range(0, parent1.shape[0]):
        if randindex[i] > 0.5:
            child1[i, :] = parent2[i, :]
            child2[i, :] = parent1[i, :]
    return np.array(child1, dtype='int'), np.array(child2, dtype='int')

'''