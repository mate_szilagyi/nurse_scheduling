def tournament_tests():
    print(calculate_maximum_fitness(get_nurses_list()))

    for i in range(0, 10):
        print(i, 'tournament one-point scramble')
        genetic_alg(selection='tournament', crossover_type='onepoint', mutation_type='scramble')
    for i in range(0, 10):
        print(i, 'tournament one-point swap')
        genetic_alg(selection='tournament', crossover_type='onepoint', mutation_type='swap')
    for i in range(0, 10):
        print(i, 'tournament one-point resetting')
        genetic_alg(selection='tournament', crossover_type='onepoint', mutation_type='resetting')
    for i in range(0, 10):
        print(i, 'tournament uniform scramble')
        genetic_alg(selection='tournament', crossover_type='uniform', mutation_type='scramble')
    for i in range(0, 10):
        print(i, 'tournament uniform swap')
        genetic_alg(selection='tournament', crossover_type='uniform', mutation_type='swap')
    for i in range(0, 10):
        print(i, 'tournament uniform resetting')
        genetic_alg(selection='tournament', crossover_type='uniform', mutation_type='resetting')


def roulette_tests():
    print(calculate_maximum_fitness(get_nurses_list()))

    for i in range(0, 10):
        print(i + 1, 'roulette one-point scramble')
        genetic_alg(selection='roulette', crossover_type='onepoint', mutation_type='scramble')
    for i in range(0, 10):
        print(i + 1, 'roulette one-point swap')
        genetic_alg(selection='roulette', crossover_type='onepoint', mutation_type='swap')
    for i in range(0, 10):
        print(i + 1, 'roulette one-point resetting')
        genetic_alg(selection='roulette', crossover_type='onepoint', mutation_type='resetting')
    for i in range(0, 10):
        print(i + 1, 'roulette uniform scramble')
        genetic_alg(selection='roulette', crossover_type='uniform', mutation_type='scramble')
    for i in range(0, 10):
        print(i + 1, 'roulette uniform swap')
        genetic_alg(selection='roulette', crossover_type='uniform', mutation_type='swap')
    for i in range(0, 10):
        print(i + 1, 'roulette uniform resetting')
        genetic_alg(selection='roulette', crossover_type='uniform', mutation_type='resetting')
