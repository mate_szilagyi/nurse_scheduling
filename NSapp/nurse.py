class Nurse:
    def __init__(self, _id, first_name, last_name, year, month, day, is_male, rank, evening_shift, night_shift,
                 min_shift, max_shift):
        self.id = _id
        self.first_name = first_name
        self.last_name = last_name
        self.year = year
        self.month = month
        self.day = day
        self.is_male = is_male
        self.rank = rank
        self.e_shift = evening_shift
        self.n_shift = night_shift
        self.min_shift = min_shift
        self.max_shift = max_shift

    def get_full_name(self):
        return self.first_name + ' ' + self.last_name
