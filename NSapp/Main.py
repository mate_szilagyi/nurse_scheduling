from ReadNurseData import get_nurses_list
from ReadNurseData import get_planning_horizon_in_days
from ReadNurseData import nurses_required_one_shift
from ReadNurseData import write_generations
from ReadNurseData import write_timetable
import Mutation
import Crossover
import Selection

import Test

import numpy as np
import time

from operator import itemgetter
import cProfile, pstats, io


def profile(fnc):
    """A decorator that uses cProfile to profile a function"""

    def inner(*args, **kwargs):
        pr = cProfile.Profile()
        pr.enable()
        retval = fnc(*args, **kwargs)
        pr.disable()
        s = io.StringIO()
        sortby = 'cumulative'
        ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
        ps.print_stats()
        print(s.getvalue())
        return retval

    return inner


def generate_random_matrix(row, col):
    patterns = [[0], [1, 0, 0], [1, 1, 0, 0, 0]]
    probability = ['0.3', '0.6', '0.1']
    m = np.zeros(shape=(row, col))
    for i in range(0, row):
        l = []
        flat_list = []
        while flat_list.__len__() < col:
            l.append(np.random.choice(patterns, p=probability))
            flat_list = [item for sublist in l for item in sublist]

        m[i] = flat_list[0:col]

    return np.array(m, dtype='int')


# return the nr of violations
def validate(matrix):
    patterns = ['101', '111', '11001']
    x = 0
    for j in range(0, matrix.shape[0]):
        s = ''.join([i for i in np.array_str(matrix[j]) if i.isdigit()])
        x = x + s.count(patterns[0])
        x = x + s.count(patterns[1])
        x = x + s.count(patterns[2])
    return x


# Calculate fitness value of matrix
# small, medium, high - refers to weights of fitness function
def calculate_fitness(matrix, nurses, required_staff):
    #small = 10
    medium = 50
    high = 100

    fit = 0
    for i in range(0, matrix.shape[0]):
        sum_ = sum(matrix[i])
        if nurses[i].min_shift <= sum_ <= nurses[i].max_shift:
            fit += medium
        if nurses[i].e_shift == sum(matrix[i][1::3]):
            fit += medium
        if nurses[i].n_shift == sum(matrix[i][2::3]):
            fit += medium
        if (nurses[i].rank == 1) and (sum_ - sum(matrix[i][0::3]) == 0):
            fit += high

    for i in range(0, matrix.shape[1]):
        sum_ = sum(matrix[:, i])
        if sum_ < required_staff:
            fit -= high
        if sum_ == required_staff:
            fit += high
        if sum_ > required_staff:
            fit += medium

    fit -= validate(matrix) * high
    if fit < 0:
        return 0
    return fit


# Calculate maximum possible fitness value a Chromosome can have
def calculate_maximum_fitness(nurses):
    #small = 10
    medium = 50
    high = 100
    h_nurse = 0
    nr_of_nurses = len(nurses)
    #staff_req = nurses_required_one_shift()
    shifts = get_planning_horizon_in_days() * 3
    for nurse in nurses:
        if nurse.rank == 1:
            h_nurse += 1

    return nr_of_nurses * medium * 3 + h_nurse * high + shifts * high


def breeding(population, nurses, staff_required, mutation_rate, crossover_rate, crossover_type, mutation_type):
    new_generation = []
    best = max(population, key=itemgetter(0))
    for i in range(0, population.__len__(), 2):
        if np.random.uniform(0, 1) < crossover_rate:
            if crossover_type == 'uniform':
                child1, child2 = Crossover.uniform(population[i][1], population[i + 1][1])
            if crossover_type == 'onepoint':
                child1, child2 = Crossover.uniform(population[i][1], population[i + 1][1])
            if np.random.uniform(0, 1) < mutation_rate:
                if mutation_type == 'swap':
                    child1 = Mutation.swap(child1)
                    child2 = Mutation.swap(child2)
                if mutation_type == 'resetting':
                    child1 = Mutation.reset(child1)
                    child2 = Mutation.reset(child2)
                if mutation_type == 'scramble':
                    child1 = Mutation.scramble(child1)
                    child2 = Mutation.scramble(child2)

            child1 = Mutation.balance(child1)
            child2 = Mutation.balance(child2)

            fit = calculate_fitness(child1, nurses, staff_required)
            new_generation.append([fit, child1])
            fit = calculate_fitness(child2, nurses, staff_required)
            new_generation.append([fit, child2])

        else:
            new_generation.append(population[i])
            new_generation.append(population[i + 1])

    i = min(new_generation, key=itemgetter(0))
    new_generation.remove(i)
    new_generation.append(best)

    return new_generation


# @profile
def genetic_alg(generations, pop_size, selection, crossover_type, mutation_type):
    start_time = time.time()
    nurses = get_nurses_list()
    planning_horizon = get_planning_horizon_in_days()
    staff_req = nurses_required_one_shift()
    population = []

    alg_data = []

    for i in range(0, pop_size):
        matrix = generate_random_matrix(nurses.__len__(), planning_horizon * 3)
        population.append([calculate_fitness(matrix, nurses, staff_req), matrix])

    for i in range(0, generations):
        if selection == 'roulette':
            selection_results = Selection.roulette_wheel(population, elite_size=int(pop_size / 10))
        if selection == 'tournament':
            selection_results = Selection.tournament(population, tournament_size=int(pop_size / 15),
                                                     elite_size=int(pop_size / 10))

        population = breeding(selection_results, nurses, staff_req, mutation_rate=0.3, crossover_rate=0.95,
                              crossover_type=crossover_type, mutation_type=mutation_type)

        best = max(population, key=itemgetter(0))
        alg_data.append([i, best[0], min(population, key=itemgetter(0))[0], np.mean(np.array(population)[:, 0])])

        print('generation: ', i, 'score:', max(population, key=itemgetter(0))[0], '  worst:',
              min(population, key=itemgetter(0))[0],
              '  mean:', int(np.mean(np.array(population)[:, 0])))

    end_time = time.time()
    print(end_time - start_time)
    # print(alg_data)
    write_generations(alg_data)
    write_timetable(nurses, best[1])


def main():
    genetic_alg(generations=200, pop_size=150, selection='tournament', crossover_type='onepoint', mutation_type='resetting')


# Test.tournament_tests()
# Test.roulette_tests()


main()
