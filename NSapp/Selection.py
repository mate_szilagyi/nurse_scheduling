import numpy as np
from operator import itemgetter

# returns the best individuals from the population
def elitism(pop, elite_size):
    elites = []
    for i in range(0, elite_size):
        best = max(pop, key=itemgetter(0))
        elites.append(best)
        pop.remove(best)
    return elites


# population: whole population that goes into tournament
# tournament_size: how many parents participate at the time in the tournament
# elite_size: nr of best individuals automatically selected
def tournament(population, tournament_size, elite_size):
    selection_size = population.__len__()
    selection_result = elitism(population, elite_size)
    population = selection_result + population
    tournament = []
    for i in range(0, selection_size - elite_size):
        rnd = list(set(np.random.randint(0, selection_size, tournament_size)))
        for j in rnd:
            tournament.append(population[j])
        selection_result.append(max(tournament, key=itemgetter(0)))
        tournament.clear()

    return selection_result


# population: whole population that goes into tournament
# elite_size: nr of best individuals automatically selected
def roulette_wheel(population, elite_size):
    selection_size = population.__len__()
    selection_result = elitism(population, elite_size)
    population = selection_result + population
    tot_sum = sum(np.array(population)[:, 0])
    min_ = min(np.array(population)[:, 0])
    population = sorted(population, key=itemgetter(0), reverse=True)
    while selection_result.__len__() < selection_size:
        r = np.random.randint(min_, tot_sum)
        partial_sum = 0
        i = 0
        while partial_sum < r:
            partial_sum += population[i][0]
            i += 1
        selection_result.append(population[i - 1])

    return selection_result
