from pathlib import Path
from nurse import Nurse
import openpyxl as xl
from datetime import datetime

nurse_data_filename = '\\nurse_data.xlsx'
path = (str(Path(__file__).resolve().parents[1]) + nurse_data_filename)
wb = xl.load_workbook(path)
sheet = wb['1week']

nurse_data_list = []
# read nurse data from excel
for row in range(2, sheet.max_row + 1):
    nurse_data_list.append(
        Nurse(sheet.cell(row, 1).value, sheet.cell(row, 2).value, sheet.cell(row, 3).value,
              sheet.cell(row, 4).value, sheet.cell(row, 5).value, sheet.cell(row, 6).value,
              sheet.cell(row, 7).value, sheet.cell(row, 8).value, sheet.cell(row, 9).value,
              sheet.cell(row, 10).value, sheet.cell(row, 11).value, sheet.cell(row, 12).value))


# PLANNING HORIZON ON field M2 in excel (for now)
def get_planning_horizon_in_days():
    return sheet['M2'].value


# Number of shifts per day field N2 in excel (for now)
def get_number_of_shifts_perday():
    return sheet['N2'].value


# return all the nurses with only relevant information for the fitness function (names not relevant)
# leaving out age and gender for now, might include later
# todo include age and gender?
def get_nurses_for_fitness():
    nurses = []
    for nurse in nurse_data_list:
        nurses.append([nurse.id, nurse.rank, nurse.e_shift, nurse.n_shift, nurse.min_shift, nurse.max_shift])
    return nurses


def get_nurses_list():
    return nurse_data_list


def nurses_required_one_shift():
    return sheet['O2'].value


def write_timetable(nurses, matrix):
    wb = xl.Workbook()
    sheet = wb.active

    cols = matrix.shape[1]
    for i in range(2, cols, 3):
        cell = sheet.cell(row=1, column=i)
        cell.value = "day {}".format(int(i / 3) + 1)
        sheet.merge_cells(start_row=1, start_column=i, end_row=1, end_column=i + 2)
        cell = sheet.cell(row=2, column=i)
        cell.value = "M"
        cell = sheet.cell(row=2, column=i + 1)
        cell.value = "E"
        cell = sheet.cell(row=2, column=i + 2)
        cell.value = "N"

    row = 3
    for nurse in nurses:
        cell = sheet.cell(row=row, column=1)
        cell.value = nurse.first_name + ' ' + nurse.last_name
        row += 1

    for i in range(0, matrix.shape[0]):
        for j in range(0, matrix.shape[1]):
            cell = sheet.cell(row=i + 3, column=j + 2)
            cell.value = matrix[i][j]
    wb.save('timetable_' + str(datetime.now().strftime("%Y-%m-%d %H%M%S")) + '.xlsx')


def write_generations(alg_data):
    wb = xl.Workbook()
    sheet = wb.active
    i = 1
    for row in alg_data:
        cell = sheet.cell(row=i, column=1)
        cell.value = row[0]

        cell = sheet.cell(row=i, column=2)
        cell.value = row[1]

        cell = sheet.cell(row=i, column=3)
        cell.value = row[2]

        cell = sheet.cell(row=i, column=4)
        cell.value = row[3]
        i += 1
    wb.save('alg_data' + str(datetime.now().strftime("%Y-%m-%d %H%M%S")) + '.xlsx')

    pass
